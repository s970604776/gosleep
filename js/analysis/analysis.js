var myBar = null,
    myHorBar = null

function createDetail(n) {
    
    document.getElementById('anaArea').innerHTML =
        '<div>' +
        '<canvas class="graph1  .d-sm-flex" id="graph2" style="margin-left: 25%"></canvas>' +
        '</div>' +
        '<div class="LevelAnalysis">' +
        'L1: Awake 昏睡 <br>' +
        'L2: REM 快速動眼期<br>' +
        'L3: Light 淺層睡眠<br>' +
        'L4: Light 淺層睡眠<br>' +
        'L5: Deep 深層睡眠' +
        '</div>' +
        '<div>' +
        '<canvas class="graph2 .d-sm-flex" id="graph1" style="margin-left: 25%"></canvas>' +
        '</div>'

    var labelsG1 = new Array(),
        dataG1 = new Array(),
        colorG1 = new Array()
    let L1 = 0,
        L2 = 0,
        L3 = 0,
        L4 = 0,
        L5 = 0
    var canvas1 = document.getElementById("graph1")
    var canvas2 = document.getElementById("graph2")


    function drawReady() {

        myBar = new Chart(canvas1, {
            type: 'bar',
            data: {
                labels: labelsG1,
                datasets: [{
                    backgroundColor: colorG1,
                    label: '睡眠震動次數',
                    data: dataG1
                }, ]
            },
            options: {
                responsive: false,
                scales: {

                    xAxes: [{
                        
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: "Page Views",
                        }
                    }]
                }
            },
            datalabels: {
                align:'end'
            }
        });
        //canvas1.style.display = "inline-block"
        myHorBar = new Chart(canvas2, {
            type: 'horizontalBar',
            data: {
                labels: ['L1', 'L2', 'L3', 'L4', 'L5'],
                datasets: [{
                    backgroundColor: ['red ', 'OrangeRed', 'MediumTurquoise ', 'LightGreen', 'Lime'],
                    label: '睡眠品質(百分比)',
                    data: [L1, L2, L3, L4, L5]
                }, ]
            },
            options: {
                responsive: false,
            }
        });
        canvas2.style.display = "initial"
        /*  if (L1 > 18)
             document.getElementById('sleepComment').innerHTML = "" */
    }
    firebase.database().ref('schedule/time/' + n).once('value', snapshot => {
        let data = snapshot.val();
        let sleepTime = snapshot.val().sleeptime;
        let wakeupTime = snapshot.val().wakeuptime;

        let tmp_i = 0;
        let y1 = sleepTime.year,
            mon1 = sleepTime.month,
            d1 = sleepTime.date,
            h1 = sleepTime.hour,
            min1 = sleepTime.minute,
            y2 = wakeupTime.year,
            mon2 = wakeupTime.month,
            d2 = wakeupTime.date,
            h2 = wakeupTime.hour,
            min2 = wakeupTime.minute
        getResponse(y1, mon1, d1, h1, min1, y2, mon2, d2, h2, min2)
        let slDate = new Date(y1 + '/' + mon1 + '/' + d1 + '/' + h1 + ':' + min1)
        let wuDate = new Date(y2 + '/' + mon2 + '/' + d2 + '/' + h2 + ':' + min2)
        let hourDiff = Math.round((wuDate.valueOf() - slDate.valueOf()) / 1000 / 60 / 60 * 100) / 100;

        while (slDate < wuDate) {
            labelsG1.push(h1 + ":" + padLeft(min1.toString(), 2))
            let old_y1 = y1,
                old_mon1 = mon1,
                old_d1 = d1,
                old_h1 = h1,
                old_min1 = min1
            y1 = y1 + Math.floor((mon1 + Math.floor(Math.floor(d1 + Math.floor(h1 + (min1 + 20) / 60) / 24) / (new Date(y1, mon1, 0).getDate()))) / 12)
            mon1 = (mon1 + Math.floor(Math.floor(d1 + Math.floor(h1 + (min1 + 20) / 60) / 24) / (new Date(y1, mon1, 0).getDate()))) % 12
            d1 = (d1 + Math.floor(Math.floor(h1 + (min1 + 20) / 60) / 24)) % (new Date(y1, mon1, 0).getDate())
            h1 = (h1 + Math.floor((min1 + 20) / 60)) % 24
            min1 = (min1 + 20) % 60
            slDate = new Date(y1 + '/' + mon1 + '/' + d1 + '/' + h1 + ':' + min1)
            let check_move = (parseInt(check(old_y1, old_mon1, old_d1, old_h1, old_min1, y1, mon1, d1, h1, min1, server_Response)))
            dataG1.push(check_move)
            if (check_move >= 10) {
                colorG1.push('red')
                L1++;
            } else if (check_move >= 7) {
                colorG1.push('OrangeRed')
                L2++
            } else if (check_move >= 6) {
                colorG1.push('MediumTurquoise')
                L3++
            } else if (check_move >= 4) {
                colorG1.push('LightGreen')
                L4++
            } else {
                colorG1.push('Lime')
                L5++
            }
        }
        if (slDate != wuDate) labelsG1.push(h2 + ":" + padLeft(min2.toString(), 2))
        let totalL = L1 + L2 + L3 + L4 + L5
        L1 = Math.floor((L1 / totalL) * 100)
        L2 = Math.floor((L2 / totalL) * 100)
        L3 = Math.floor((L3 / totalL) * 100)
        L4 = Math.floor((L4 / totalL) * 100)
        L5 = Math.floor((L5 / totalL) * 100)

        var comment = $("#commentsText");

        if(L5 > 18  & hourDiff > 8){
            comment.text("睡眠質素很好。有充足的睡眠時間，經歷了十分充實的深層睡眠，會是精神充沛的一天。");
            if(hourDiff > 9.5)  comment.text("雖然今天的睡眠質素良好，但請注意不要過度睡眠喔，因為過度睡眠可能會對身體產生負作用喔。")
            else if(L2 > 18) comment.text("有充足的時間在REM階段，今天可能會有意想不到的創作力喔!")
        }
        else if(L5 > 5 & hourDiff > 7.5){
            comment.text("睡眠質素不錯。有充足的睡眠時間，是一天好的開始喔。")
        }
        else{
            if(L5 < 5 & hourDiff > 7.5){
                comment.text("睡眠質素差。雖然睡眠時間充足，可惜進入深層睡眠時間佔比過低。可能是受生活作息不規律或是生活壓力的影響，建議在這兩方面注意一下喔。")
            }
            else if(hourDiff > 5.5 & hourDiff < 7.5){
                comment.text("睡眠素質差。睡眠時間不足，建議給予更多時間在睡眠休息的時間段幫助舒緩壓力和恢復體力喔。")
            }
            else if(hourDiff < 5.5 & hourDiff > 4){
                comment.text("睡眠素質很差。睡眠時間嚴重不足，建議給予更多時間在睡眠休息的時間段幫助舒緩壓力和恢復體力喔。")
            }
            else comment.text("睡眠素質差。建議多留意一下自已的生活作息。")
        }
        console.log(hourDiff,L5)

        console.log(labelsG1, dataG1)
        drawReady()
    })

    $("#analysisLI").click()
}